const db = require('../config/db');
const app = require('../app');

const PORT = process.env.PORT || 8080;

db.then(() => {
  app.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
  });
}).catch((error) => {
  console.log(`Server not run. Error: ${error.message}`);
});
