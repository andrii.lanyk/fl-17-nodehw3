const {Schema, model} = require('mongoose');

const messageSchema = new Schema(
  {
    message: {
      type: String,
      required: [true, 'Message is required'],
    },
    time: {
      type: Date,
      default: new Date().toString(),
    },
  },
  {_id: false},
);

const Message = model('Massage', messageSchema);

module.exports = Message;
