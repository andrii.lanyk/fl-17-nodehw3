const {Schema, model} = require('mongoose');

const userSchema = new Schema(
  {
    email: {
      type: String,
      required: [true, 'Email is required'],
      unique: true,
    },
    password: {
      type: String,
      required: [true, 'Password is required'],
    },
    role: {
      type: String,
      required: [true, 'Role is required'],
    },
    createdDate: {
      type: Date,
      default: new Date().toString(),
    },
  },
  {
    versionKey: false,
  },
);

const User = model('user', userSchema);

module.exports = User;
