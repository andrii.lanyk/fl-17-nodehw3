const {Schema, model} = require('mongoose');

const truckSchema = new Schema(
  {
    created_by: {
      type: String,
      required: [true, 'Created by is required'],
    },
    assigned_to: {
      type: String,
      default: '',
    },
    type: {
      type: String,
      required: [true, 'Type is required'],
    },
    status: {
      type: String,
      default: 'IS',
    },
    created_date: {
      type: Date,
      default: new Date().toString(),
    },
  },
  {versionKey: false},
);

const Truck = (module.exports = model('Truck', truckSchema));

module.exports = Truck;
