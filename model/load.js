const { Schema, model } = require('mongoose');
const Message = require('../model/message');
const { LoadStatus } = require('../config/constants');

const loadSchema = new Schema(
	{
		created_by: {
			type: String,
			required: [true, 'Created by is required'],
		},
		assigned_to: {
			type: String,
			default: '',
		},
		status: {
			type: String,
			default: LoadStatus.NEW,
		},
		state: {
			type: String,
			default: '',
		},
		name: {
			type: String,
			required: [true, 'Name is required'],
		},
		payload: {
			type: Number,
			required: [true, 'Payload is required'],
		},
		pickup_address: {
			type: String,
			required: [true, 'Pickup address is required'],
		},
		delivery_address: {
			type: String,
			required: [true, 'Delivery address is required'],
		},
		dimensions: {
			width: {
				type: Number,
				required: [true, 'Width is required'],
			},
			length: {
				type: Number,
				required: [true, 'Length is required'],
			},
			height: {
				type: Number,
				required: [true, 'Height is required'],
			},
		},
		messages: [Message.schema],
		created_date: {
			type: Date,
			default: new Date().toString(),
		},
	},
	{ versionKey: false },
);

const Load = model('Load', loadSchema);

module.exports = Load;
