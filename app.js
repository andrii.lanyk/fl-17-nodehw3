const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const boolParser = require('express-query-boolean');
const helmet = require('helmet');
const {HttpCode} = require('./config/constants');

const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
const loadRouter = require('./routes/load');
const truckRouter = require('./routes/truck');

const app = express();

const formatsLogger = app.get('env') === 'development' ? 'dev' : 'short';

app.use(helmet());
app.use(logger(formatsLogger));
app.use(cors());
app.use(express.json({limit: 10000}));
app.use(boolParser());

app.use('/api/users/me', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/loads', loadRouter);
app.use('/api/trucks', truckRouter);

app.use((_req, res) => {
  res.status(HttpCode.BAD_REQUEST).json({message: 'Not found'});
});

app.use((err, _req, res, _next) => {
  res.status(HttpCode.INTERNAL_SERVER_ERROR).json({message: err.message});
});

module.exports = app;
