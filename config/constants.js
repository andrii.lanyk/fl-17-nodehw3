const Role = {
  DRIVER: 'DRIVER',
  SHIPPER: 'SHIPPER',
};

const HttpCode = {
  OK: 200,
  CREATED: 201,
  ACCEPTED: 202,
  NO_CONTENT: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  TOO_MANY_REQUESTS: 429,
  INTERNAL_SERVER_ERROR: 500,
};

const SALT_FACTOR = 6;

const TruckTypes = {
  SPRINTER: 'SPRINTER',
  SMALL: 'SMALL STRAIGHT',
  LARGE: 'LARGE STRAIGHT',
};

const TruckTypesConfig = {
  // eslint-disable-next-line quote-props
  SPRINTER: {
    payload: 2100,
    dimensions: {
      width: 250,
      length: 300,
      height: 190,
    },
  },
  'SMALL STRAIGHT': {
    payload: 3200,
    dimensions: {
      width: 320,
      length: 350,
      height: 210,
    },
  },
  'LARGE STRAIGHT': {
    payload: 4500,
    dimensions: {
      width: 360,
      length: 410,
      height: 240,
    },
  },
};

const LoadStates = [
  'In the road to pick-up address',
  'Arrived to pick-up address',
  'In the road to delivery address',
  'Arrived to delivery address',
];

const LoadStatus = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

module.exports = {
  Role,
  HttpCode,
  SALT_FACTOR,
  TruckTypes,
  TruckTypesConfig,
  LoadStates,
  LoadStatus,
};
