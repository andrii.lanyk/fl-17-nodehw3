const bcrypt = require('bcryptjs');
const {SALT_FACTOR} = require('./constants');

const passwordCrypt = async (password) => {
  const salt = await bcrypt.genSalt(SALT_FACTOR);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

const comparePassword = async (newPassword, password) => {
  return await bcrypt.compare(newPassword, password);
};

module.exports = {
  passwordCrypt,
  comparePassword,
};
