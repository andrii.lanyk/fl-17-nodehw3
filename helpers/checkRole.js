const {Role, HttpCode} = require('../config/constants');

const checkRole = (roleToCheck) => {
  return (req, res, next) => {
    const {role} = req.decoded;
    if (role !== roleToCheck) {
      return res.status(HttpCode.BAD_REQUEST).json({
        message: 'You don`t have permission to do this',
      });
    }
    next();
  };
};

const checkDriver = checkRole(Role.DRIVER);

const checkShipper = checkRole(Role.SHIPPER);

module.exports = {
  checkDriver,
  checkShipper,
};
