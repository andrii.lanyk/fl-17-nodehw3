const { comparePassword } = require('../config/passport');
const Users = require('../repository/users');
const { HttpCode } = require('../config/constants');

const checkPassword = async (req, res, next) => {
	const password = req.body.password || req.body.oldPassword;
	const email = req.body.email || req.decoded.email;
	const user = await Users.getByEmail(email);
	const isCorrectPassword = await comparePassword(password, user.password);
	if (!isCorrectPassword) {
		return res
			.status(HttpCode.BAD_REQUEST)
			.json({ message: 'Password is wrong' });
	}
	next();
};

module.exports = {
	checkPassword,
};
