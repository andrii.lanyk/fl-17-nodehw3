const { Types } = require('mongoose');
const { HttpCode } = require('../config/constants');

const checkId = async (req, res, next) => {
	const { id } = req.params;
	try {
		const correctId = Types.ObjectId.isValid(id);
		if (!correctId) {
			return res
				.status(HttpCode.BAD_REQUEST)
				.json({ message: 'Such id isn`t correct' });
		}
		next();
	} catch (error) {
		console.log('error in checkId: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

module.exports = {
	checkId,
};
