const jwt = require('jsonwebtoken');
const { HttpCode } = require('../config/constants');
require('dotenv').config();
const SECRET_KEY = process.env.JWT_SECRET_KEY;

const createToken = (user) => jwt.sign(user, SECRET_KEY, { expiresIn: '6h' });

const verifyToken = (req, res, next) => {
	try {
		const token = req.headers.authorization?.includes('JWT')
			? req.headers.authorization?.split(' ')[1]
			: req.headers.authorization;
		if (token) {
			jwt.verify(token, SECRET_KEY, (error, decoded) => {
				if (error) {
					return res
						.status(HttpCode.BAD_REQUEST)
						.json({ message: 'Credentials is not valid' });
				}
				req.decoded = decoded;
				next();
			});
		} else {
			res
				.status(HttpCode.BAD_REQUEST)
				.json({ message: 'Credentials is not valid' });
		}
	} catch (error) {
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

module.exports = {
	createToken,
	verifyToken,
};
