const User = require('../model/user');

const getByEmail = async (email) => {
  return await User.findOne({email});
};

const addUser = async (user) => {
  return await user.save();
};

const changePassword = (_id, password) => {
  return User.findByIdAndUpdate({_id}, {password});
};

const deleteUser = async (id) => {
  return await User.findByIdAndRemove(id);
};

module.exports = {
  getByEmail,
  addUser,
  changePassword,
  deleteUser,
};
