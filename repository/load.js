const Load = require('../model/load');
const {Role, LoadStatus, LoadStates} = require('../config/constants');

const createLoad = async (load) => {
  return await load.save();
};

const getLoad = async (_id, userId) => {
  return await Load.findOne({_id, created_by: userId});
};

const getNewLoad = async (_id, userId) => {
  const filter = {_id, created_by: userId, status: LoadStatus.NEW};
  return await Load.findOne(filter);
};

const getActiveLoad = async (userId) => {
  return await Load.findOne({
    $or: [{status: LoadStatus.ASSIGNED}, {status: LoadStatus.POSTED}],
    $and: [{assigned_to: userId}],
  });
};

const getFilteredLoads = async (userId, role, status, limit, offset) => {
  const filter = {};
  if (status) {
    filter.status = status;
  }
  if (role === Role.SHIPPER) {
    filter.created_by = userId;
  }
  if (role === Role.DRIVER) {
    filter.assigned_to = userId;
  }
  return await Load.find(filter).skip(offset).limit(limit);
};

const updateLoad = async (_id, userId, role, updateData) => {
  let filter;
  if (role === Role.SHIPPER) {
    filter = {_id, created_by: userId};
  }
  if (role === Role.DRIVER) {
    filter = {_id, assigned_to: userId};
  }
  return await Load.findOneAndUpdate(filter, updateData);
};

const deleteLoad = async (_id, userId) => {
  const filter = {_id, created_by: userId};
  return await Load.findOneAndDelete(filter);
};

const deleteUsersLoads = async (userId) => {
  return await Load.deleteMany({created_by: userId});
};

const getNextStateLoad = (currentState) => {
  return LoadStates[
    LoadStates.findIndex((state) => state === currentState) + 1
  ];
};

module.exports = {
  createLoad,
  getLoad,
  getNewLoad,
  getActiveLoad,
  getFilteredLoads,
  updateLoad,
  deleteLoad,
  deleteUsersLoads,
  getNextStateLoad,
};
