const Truck = require('../model/truck');
const {truckTypesConfig} = require('../config/constants');

const getTrucks = async (userId) => {
  return await Truck.find({created_by: userId});
};

const getFreeTrucks = async () => {
  return await Truck.find({assigned_to: {$ne: ''}, status: 'IS'});
};

const getTruck = async (_id, userId) => {
  return await Truck.findOne({_id, created_by: userId});
};

const createTruck = async (truck) => {
  return await truck.save();
};

const assignTruck = async (_id, userId) => {
  const filter = {_id, created_by: userId};
  return await Truck.findOneAndUpdate(filter, {assigned_to: userId});
};

const getAssignedTruck = async (userId) => {
  return await Truck.findOne({assigned_to: userId});
};

const getNoAssignedTruck = async (_id, userId) => {
  const filter = {_id, created_by: userId, assigned_to: ''};
  return await Truck.findOne(filter);
};

const updateTruck = async (_id, userId, type) => {
  const filter = {_id, created_by: userId};
  return await Truck.findOneAndUpdate(filter, {type});
};

const updateTruckStatus = async (userId, status) => {
  return await Truck.findOneAndUpdate({assigned_to: userId}, {status});
};

const updateTruckBesidesSelected = async (_id, userId) => {
  const filter = {
    _id: {$nin: _id},
    created_by: userId,
    assigned_to: userId,
  };
  return await Truck.findOneAndUpdate(filter, {assigned_to: ''});
};

const deleteTruck = async (_id, userId) => {
  const filter = {_id, created_by: userId};
  return await Truck.findOneAndDelete(filter);
};

const deleteUsersTrucks = async (userId) => {
  return await Truck.deleteMany({created_by: userId});
};

const findDriverWithoutLoad = async (load) => {
  const freeTrucks = await getFreeTrucks();
  const freeDriver = freeTrucks.find((truck) => {
    const type = truck.type;
    return (
      load.payload <= truckTypesConfig[type].payload &&
      load.size.width <= truckTypesConfig[type].size.width &&
      load.size.length <= truckTypesConfig[type].size.length &&
      load.size.height <= truckTypesConfig[type].size.height
    );
  });
  return freeDriver;
};

module.exports = {
  getTrucks,
  getFreeTrucks,
  getTruck,
  createTruck,
  assignTruck,
  getAssignedTruck,
  getNoAssignedTruck,
  updateTruck,
  updateTruckStatus,
  updateTruckBesidesSelected,
  deleteTruck,
  deleteUsersTrucks,
  findDriverWithoutLoad,
};
