const Message = require('../model/message');

const addMessage = async (newMessage, load) => {
  return await load.updateOne({$push: {messages: newMessage}});
};

const deleteMessage = async (_id, load) => {
  return await load.updateOne({$pull: {messages: {_id}}});
};

const addNewMessage = async (load, message) => {
  const newMessage = new Message({message});
  await addMessage(newMessage, load);
};

module.exports = {addMessage, deleteMessage, addNewMessage};
