const Users = require('../repository/users');
const User = require('../model/user');
const { createToken } = require('../helpers/token');
const { passwordCrypt } = require('../config/passport');
const { HttpCode } = require('../config/constants');

const registration = async (req, res, _next) => {
	const { email, password, role } = req.body;

	try {
		await Users.addUser(
			new User({ email, password: await passwordCrypt(password), role }),
		);
		res
			.status(HttpCode.OK)
			.json({ message: 'User`s profile was created successfully' });
	} catch (error) {
		console.log('error: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const login = async (req, res, _next) => {
	const { email } = req.body;
	try {
		const user = await Users.getByEmail(email);
		const token = createToken(user.toJSON());
		res.status(HttpCode.OK).json({ jwt_token: token });
	} catch (error) {
		console.log('error: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const resetPassword = (_req, res, _next) => {
	res.status(HttpCode.OK).json({
		message: 'New password was sent to your email address',
	});
};

module.exports = {
	registration,
	login,
	resetPassword,
};
