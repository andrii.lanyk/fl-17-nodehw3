const Load = require('../model/load');
const Loads = require('../repository/load');
const Trucks = require('../repository/trucks');
const { addNewMessage } = require('../repository/message');
const { findDriverWithoutLoad } = require('../repository/trucks');
const { getNextStateLoad } = require('../repository/load');
const {
	Role,
	HttpCode,
	LoadStatus,
	LoadStates,
} = require('../config/constants');

const getLoads = async (req, res, _next) => {
	const userId = req.decoded._id;
	const role = req.decoded.role;
	const { status, limit = 10, offset = 0 } = req.query;
	try {
		if (role === Role.DRIVER && status === LoadStatus.NEW) {
			return res.status(HttpCode.BAD_REQUEST).json({
				message: 'Driver can`t have loads with status new',
			});
		}
		const loads = await Loads.getFilteredLoads(
			userId,
			role,
			status,
			limit,
			offset,
		);
		res.status(HttpCode.OK).json({ loads });
	} catch (error) {
		console.log('error in getLoads: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const addLoad = async (req, res, _next) => {
	const userId = req.decoded._id;
	const loadData = req.body;
	try {
		const newLoad = new Load({
			created_by: userId,
			...loadData,
		});
		await Loads.createLoad(newLoad);
		res.status(HttpCode.OK).json({ message: 'Load was created successfully' });
	} catch (error) {
		console.log('error in addLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const getActiveLoad = async (req, res, _next) => {
	const userId = req.decoded._id;
	try {
		const load = (await Loads.getActiveLoad(userId)) || {};
		res.status(HttpCode.OK).json({ load });
	} catch (error) {
		console.log('error in getActiveLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const updateStateLoad = async (req, res, _next) => {
	const userId = req.decoded._id;
	const role = req.decoded.role;
	try {
		const load = await Loads.getActiveLoad(userId);
		if (!load) {
			return res
				.status(HttpCode.BAD_REQUEST)
				.json({ message: 'We don`t have active load' });
		}
		const nextState = getNextStateLoad(load.state);
		await Load.updateLoad(load._id, userId, role, { state: nextState });
		const message = `State's load was changed to '${nextState}'`;
		await addNewMessage(load, message);
		if (nextState === LoadStates[LoadStates.length - 1]) {
			await Loads.updateLoad(load._id, userId, role, {
				status: LoadStatus.SHIPPED,
			});
			await addNewMessage(load, 'Load`s status was changed to shipped');
			await Trucks.updateTruckStatus(userId, 'IS');
		}
		res.status(HttpCode.OK).json({ message });
	} catch (error) {
		console.log('error in updateStateLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const getLoad = async (req, res, _next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const load = await Loads.getLoad(id, userId);
		res.status(HttpCode.OK).json({ load });
	} catch (error) {
		console.log('error in getLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const updateLoad = async (req, res, _next) => {
	const userId = req.decoded._id;
	const role = req.decoded.role;
	const { id } = req.params;
	const loadData = req.body;
	try {
		await Loads.updateLoad(id, userId, role, loadData);
		res
			.status(HttpCode.OK)
			.json({ message: 'Information about load was changed successfully' });
	} catch (error) {
		console.log('error in updateLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const deleteLoad = async (req, res) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		await Loads.deleteLoad(id, userId);
		res.status(HttpCode.OK).json({ message: 'Load deleted successfully' });
	} catch (error) {
		console.log('error in deleteLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const postLoad = async (req, res) => {
	const userId = req.decoded._id;
	const role = req.decoded.role;
	const { id } = req.params;
	try {
		const load = await Loads.updateLoad(id, userId, role, {
			status: LoadStatus.POSTED,
		});
		await addNewMessage(load, 'Load was posted successfully');
		const freeDriver = await findDriverWithoutLoad(load);
		if (!freeDriver) {
			const message = 'Driver was not found, load status was changed to new';
			await Loads.updateLoad(id, userId, role, { status: LoadStatus.NEW });
			await addNewMessage(load, message);
			return res.status(HttpCode.BAD_REQUEST).json({ message });
		}
		const driverId = freeDriver.created_by;
		const message = `Load was assigned to driver with id ${driverId}`;
		await Trucks.updateTruckStatus(driverId, 'OL');
		const loadData = {
			assigned_to: driverId,
			state: LoadStates[0],
			status: LoadStatus.ASSIGNED,
		};
		await Loads.updateLoad(id, userId, role, loadData);
		await addNewMessage(load, message);
		res.status(HttpCode.OK).json({
			message: 'Load was posted successfully',
			driver_found: true,
		});
	} catch (error) {
		console.log('error in postLoad: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const getShippingLoadInfo = async (req, res) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const load = await Loads.getLoad(id, userId);
		const truck = await Trucks.getAssignedTruck(load.assigned_to);
		res.status(HttpCode.OK).json({ load, truck });
	} catch (error) {
		console.log('error in getShippingLoadInfo: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

module.exports = {
	getLoads,
	addLoad,
	getActiveLoad,
	updateStateLoad,
	getLoad,
	updateLoad,
	deleteLoad,
	postLoad,
	getShippingLoadInfo,
};
