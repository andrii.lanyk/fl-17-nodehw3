const Users = require('../repository/users');
const Trucks = require('../repository/trucks');
const Loads = require('../repository/load');
const { Role, HttpCode } = require('../config/constants');
const { passwordCrypt } = require('../config/passport');

const getInfo = (req, res, _next) => {
	const { _id, role, email, createdDate } = req.decoded;
	res.status(HttpCode.OK).json({ user: { _id, role, email, createdDate } });
};

const deleteProfile = async (req, res, _next) => {
	const { _id, role } = req.decoded;
	try {
		await Users.deleteUser(_id);
		if (role === Role.SHIPPER) {
			await Loads.deleteUsersLoads(_id);
		}
		if (role === Role.DRIVER) {
			await Trucks.deleteUsersTrucks(_id);
		}
		res
			.status(HttpCode.OK)
			.json({ message: 'User`s profile was deleted successfully' });
	} catch (error) {
		console.log('error in deleteProfile: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const changePassword = async (req, res, _next) => {
	const { _id } = req.decoded;
	const { newPassword } = req.body;
	try {
		const saltPassword = await passwordCrypt(newPassword);
		await Users.changePassword(_id, saltPassword);
		res
			.status(HttpCode.OK)
			.json({ message: 'User`s password was changed successfully' });
	} catch (error) {
		console.log('error in changePassword: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

module.exports = {
	getInfo,
	deleteProfile,
	changePassword,
};
