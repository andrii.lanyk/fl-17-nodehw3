const Truck = require('../model/truck');
const Trucks = require('../repository/trucks');
const { HttpCode } = require('../config/constants');

const getTrucks = async (req, res, _next) => {
	const userId = req.decoded._id;
	try {
		const trucks = await Trucks.getTrucks(userId);
		res.status(HttpCode.OK).json({ trucks });
	} catch (error) {
		console.log('error in getTrucks: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const addTruck = async (req, res, _next) => {
	const userId = req.decoded._id;
	const { type } = req.body;
	try {
		const newTruck = new Truck({
			created_by: userId,
			type,
		});
		await Trucks.createTruck(newTruck);
		res.status(HttpCode.OK).json({ message: 'Truck was created successfully' });
	} catch (error) {
		console.log('error in addTruck: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const getTruck = async (req, res, _next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const truck = await Trucks.getTruck(id, userId);
		res.status(HttpCode.OK).json({ truck });
	} catch (error) {
		console.log('error in getTruck: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const assignTruck = async (req, res) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		await Trucks.assignTruck(id, userId);
		await Trucks.updateTruckBesidesSelected(id, userId);
		res
			.status(HttpCode.OK)
			.json({ message: 'Truck was assigned successfully' });
	} catch (error) {
		console.log('error in assignTruck: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const updateTruck = async (req, res, _next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	const { type } = req.body;
	try {
		await Trucks.updateTruck(id, userId, type);
		res
			.status(HttpCode.OK)
			.json({ message: 'Information about truck was changed successfully' });
	} catch (error) {
		console.log('error in updateTruck: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

const deleteTruck = async (req, res, _next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		await Trucks.deleteTruck(id, userId);
		res.status(HttpCode.OK).json({ message: 'Truck was deleted successfully' });
	} catch (error) {
		console.log('error in deleteTruck: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

module.exports = {
	getTrucks,
	addTruck,
	getTruck,
	assignTruck,
	updateTruck,
	deleteTruck,
};
