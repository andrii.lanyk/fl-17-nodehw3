const Joi = require('joi');
const Trucks = require('../../repository/trucks');
const { HttpCode, TruckTypes } = require('../../config/constants');

const truckTypeSchema = Joi.object().keys({
	type: Joi.string()
		.valid(TruckTypes.SPRINTER, TruckTypes.SMALL, TruckTypes.LARGE)
		.required(),
});

const truckValidation = async (req, res, next) => {
	const data = req.body;
	try {
		await truckTypeSchema.validateAsync(data);
		next();
	} catch (error) {
		console.log('error in truckValidation: ', error);
		const message = error.details[0].message;
		res.status(HttpCode.BAD_REQUEST).json({ message });
	}
};

const truckExistValidation = async (req, res, next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const truck = await Trucks.getTruck(id, userId);
		if (!truck) {
			return res
				.status(HttpCode.BAD_REQUEST)
				.json({ message: 'We don`t have such truck' });
		}
		next();
	} catch (error) {
		console.log('error in truckExistValidation: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

const truckAssignedValidation = async (req, res, next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const truck = await Trucks.getNoAssignedTruck(id, userId);
		if (!truck) {
			return res.status(HttpCode.BAD_REQUEST).json({
				message: 'This truck is already assigned',
			});
		}
		next();
	} catch (error) {
		console.log('error in truckAssignedValidation: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

module.exports = {
	truckValidation,
	truckExistValidation,
	truckAssignedValidation,
};
