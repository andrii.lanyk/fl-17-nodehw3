const express = require('express');
const router = new express.Router();
const {
  getTrucks,
  addTruck,
  getTruck,
  assignTruck,
  updateTruck,
  deleteTruck,
} = require('../../controllers/truck');
const {verifyToken} = require('../../helpers/token');
const {checkDriver} = require('../../helpers/checkRole');
const {
  truckValidation,
  truckExistValidation,
  truckAssignedValidation,
} = require('./validation');
const {checkId} = require('../../helpers/checkId');

router.get('/', verifyToken, checkDriver, getTrucks);

router.post('/', verifyToken, checkDriver, truckValidation, addTruck);

router.get(
  '/:id',
  verifyToken,
  checkDriver,
  checkId,
  truckExistValidation,
  getTruck,
);

router.put(
  '/:id',
  verifyToken,
  checkDriver,
  checkId,
  truckValidation,
  truckExistValidation,
  truckAssignedValidation,
  updateTruck,
);

router.delete(
  '/:id',
  verifyToken,
  checkDriver,
  checkId,
  truckExistValidation,
  truckAssignedValidation,
  deleteTruck,
);

router.post(
  '/:id/assign',
  verifyToken,
  checkDriver,
  checkId,
  truckExistValidation,
  truckAssignedValidation,
  assignTruck,
);

module.exports = router;
