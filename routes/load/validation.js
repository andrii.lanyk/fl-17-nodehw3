const Joi = require('joi');
const Loads = require('../../repository/load');
const { HttpCode, LoadStatus } = require('../../config/constants');

const loadSchema = Joi.object().keys({
	name: Joi.string().required(),
	payload: Joi.number().required(),
	pickup_address: Joi.string().required(),
	delivery_address: Joi.string().required(),
	dimensions: Joi.object()
		.keys({
			width: Joi.number().required(),
			length: Joi.number().required(),
			height: Joi.number().required(),
		})
		.required(),
});

const loadStatusSchema = Joi.string().valid(
	LoadStatus.NEW,
	LoadStatus.POSTED,
	LoadStatus.ASSIGNED,
	LoadStatus.SHIPPED,
);

const loadValidation = async (req, res, next) => {
	const data = req.body;
	try {
		await loadSchema.validateAsync(data);
		next();
	} catch (error) {
		console.log('error in loadValidation: ', error);
		const message = error.details[0].message;
		res.status(HttpCode.BAD_REQUEST).json({ message });
	}
};

const loadExistValidation = async (req, res, next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const load = await Loads.getLoad(id, userId);
		if (!load) {
			return res
				.status(HttpCode.BAD_REQUEST)
				.json({ message: 'Load not found' });
		}
		next();
	} catch (error) {
		console.log('error in loadExistValidation: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

const loadStatusValidation = async (req, res, next) => {
	const { status } = req.query;
	try {
		await loadStatusSchema.validateAsync(status);
		next();
	} catch (error) {
		console.log('error in loadStatusValidation: ', error);
		const message = error.details[0].message;
		res.status(HttpCode.BAD_REQUEST).json({ message });
	}
};

const loadNewValidation = async (req, res, next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const load = await Loads.getNewLoad(id, userId);
		if (!load) {
			return res.status(HttpCode.BAD_REQUEST).json({
				message: 'Such thing is possible only with new load',
			});
		}
		next();
	} catch (error) {
		console.log('error in loadNewValidation: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

const loadActiveValidation = async (req, res, next) => {
	const userId = req.decoded._id;
	const { id } = req.params;
	try {
		const load = await Loads.getLoad(id, userId);
		if (
			load.status !== LoadStatus.ASSIGNED &&
			load.status !== LoadStatus.POSTED
		) {
			return res.status(HttpCode.BAD_REQUEST).json({
				message: 'Such thing is possible only with active load',
			});
		}
		next();
	} catch (error) {
		console.log('error in loadActiveValidation: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error!' });
	}
};

module.exports = {
	loadValidation,
	loadExistValidation,
	loadStatusValidation,
	loadNewValidation,
	loadActiveValidation,
};
