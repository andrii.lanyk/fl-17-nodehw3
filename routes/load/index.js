const express = require('express');
const router = new express.Router();
const {
  getLoads,
  addLoad,
  getActiveLoad,
  updateStateLoad,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingLoadInfo,
} = require('../../controllers/load');
const {verifyToken} = require('../../helpers/token');
const {checkDriver, checkShipper} = require('../../helpers/checkRole');
const {checkId} = require('../../helpers/checkId');
const {
  loadValidation,
  loadExistValidation,
  loadStatusValidation,
  loadNewValidation,
  loadActiveValidation,
} = require('./validation');

router.get('/', verifyToken, loadStatusValidation, getLoads);

router.post('/', verifyToken, checkShipper, loadValidation, addLoad);

router.get('/active', verifyToken, checkDriver, getActiveLoad);

router.patch('/active/state', verifyToken, checkDriver, updateStateLoad);

router.get('/:id', verifyToken, checkId, loadExistValidation, getLoad);

router.put(
  '/:id',
  verifyToken,
  checkShipper,
  checkId,
  loadValidation,
  loadExistValidation,
  loadNewValidation,
  updateLoad,
);

router.delete(
  '/:id',
  verifyToken,
  checkShipper,
  checkId,
  loadExistValidation,
  loadNewValidation,
  deleteLoad,
);

router.post(
  '/:id/post',
  verifyToken,
  checkShipper,
  checkId,
  loadExistValidation,
  loadNewValidation,
  postLoad,
);

router.get(
  '/:id/shipping_info',
  verifyToken,
  checkShipper,
  checkId,
  loadExistValidation,
  loadActiveValidation,
  getShippingLoadInfo,
);

module.exports = router;
