const Joi = require('joi');
const {HttpCode} = require('../../config/constants');

const changePasswordSchema = Joi.object().keys({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
});

const userValidation = async (req, res, next) => {
  const data = req.body;
  try {
    await changePasswordSchema.validateAsync(data);
    next();
  } catch (error) {
    const message = error.details[0].message;
    res.status(HttpCode.BAD_REQUEST).json({message});
  }
};

module.exports = {
  userValidation,
};
