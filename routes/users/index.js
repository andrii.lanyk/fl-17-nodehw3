const express = require('express');
const router = new express.Router();
const {
  getInfo,
  deleteProfile,
  changePassword,
} = require('../../controllers/users');
const {userValidation} = require('./validation');
const {verifyToken} = require('../../helpers/token');
const {checkPassword} = require('../../helpers/checkPassword');

router.get('/', verifyToken, getInfo);

router.delete('/', verifyToken, deleteProfile);

router.patch(
  '/password',
  verifyToken,
  userValidation,
  checkPassword,
  changePassword,
);

module.exports = router;
