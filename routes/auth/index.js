const express = require('express');
const router = new express.Router();
const {registration, login, resetPassword} = require('../../controllers/auth');
const {checkPassword} = require('../../helpers/checkPassword');
const {validateAuth, validateUser} = require('./validation');

router.post('/register', validateAuth, validateUser, registration);

router.post('/login', validateAuth, validateUser, checkPassword, login);

router.post('/forgot_password', validateAuth, validateUser, resetPassword);

module.exports = router;
