const Joi = require('joi');
const User = require('../../repository/users');
const { Role, HttpCode } = require('../../config/constants');

const registerSchema = Joi.object().keys({
	email: Joi.string().email().required(),
	password: Joi.string().required(),
	role: Joi.string().valid(Role.SHIPPER, Role.DRIVER).required(),
});

const loginSchema = Joi.object().keys({
	email: Joi.string().email().required(),
	password: Joi.string().required(),
});

const resetPasswordSchema = Joi.object().keys({
	email: Joi.string().email().required(),
});

const validateAuth = async (req, res, next) => {
	const data = req.body;
	const url = req.url;
	try {
		switch (url) {
			case '/register':
				await registerSchema.validateAsync(data);
				next();
				break;
			case '/login':
				await loginSchema.validateAsync(data);
				next();
				break;
			case '/forgot_password':
				await resetPasswordSchema.validateAsync(data);
				next();
				break;
			default:
				next();
		}
	} catch (error) {
		console.log('err in validateAuth: ', err);
		const message = error.details[0].message;
		res.status(HttpCode.BAD_REQUEST).json({ message });
	}
};

const validateUser = async (req, res, next) => {
	const { email } = req.body;
	const url = req.url;
	try {
		let message;
		const user = await User.getByEmail(email);
		if (user && url === '/register') {
			message = 'You already have an account';
		}
		if (!user && (url === '/login' || url === '/forgot_password')) {
			message = 'User not found';
		}
		if (message) {
			return res.status(HttpCode.BAD_REQUEST).json({ message });
		}
		next();
	} catch (error) {
		console.log('err in validateUser: ', error);
		res
			.status(HttpCode.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal Server Error!' });
	}
};

module.exports = {
	validateAuth,
	validateUser,
};
